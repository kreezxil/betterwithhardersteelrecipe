[![Bisect Hosting](https://www.bisecthosting.com/images/logos/dark_text@1538x500.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil)

If you want a server setup for the Any mod pack with zero effort, get a [server with BisectHosting](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil) and receive 25% off your first month as a new customer using the code kreezxil

## Description

When playing Better With Mods and you have the HarderSteelRecipe enabled crafting a simple stone button becomes a task that involves going to The End to get Endstone that is required to make Ender Slag and a whole slew of other items so you can eventually make a stone cutter to make the stone button. This mod gets rid of that effect, and allows you to craft a stone button by making button clay, forming that into an unfired stone button and then finally cooking that to get a stone button.

Currently this is all that the mod provides. I see no reason to circumvent the other recipes provided by Better With Mods as they are all easily craftable compared to the asinine sequence involved with the stone button.

## Recipes

---

See the [wiki](https://minecraft.curseforge.com/projects/better-with-harder-steel-recipe/pages/wiki) for the amazing recipes!

This mod is now coded in standard McJty-Style.

### Note

If we begin to add features to this then I'll revisit the 1.11.2 version and recode it as well.

## Requests

---

Because we all like the steel recipes but don't like some of the changes that comes with it, to include even the HCRedstone and possibly even the HCLumber, I am willing to take requests that give an alternative to some of the insane recipes for more sensible ones.

When you do submit your request I want you to consider that BWM is a slow progression mod at its core not a Windmill Technology mod. Therefore, submit your requests show with an easier progression chain than that being forced upon us.

An example would be the Piston requires a soul urn, let's replace that with a piston rod that is crafted with 3 iron ingots vertical in the center with slime on the sides. This iron rod would go where the original iron ingot went in the piston recipe. <--- recipe not  yet added

## Modpacks

Yes you can use my mod in your pack.

## Help a Veteran today

I am Veteran of United States Army. I am not disabled. But I do love to make these mods and modpacks for you guys. Please help me to help you by Donating at [![](https://i.imgur.com/GCR1VaN.png)](https://patreon.com/kreezxil)[https://patreon.com/kreezxil](https://patreon.com/kreezxil) .

**This project is proudly powered by FORGE**, without whom it would not be possible.Help FORGE get rid of Adfocus at [https://www.patreon.com/LexManos](https://www.patreon.com/LexManos).
